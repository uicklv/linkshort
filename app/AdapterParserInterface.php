<?php


namespace App;


interface AdapterParserInterface
{

    public function getBrowser();

    public function getEngine();

    public function getOs();

    public function getDevice();
}
