<?php


namespace App;

class WhichAdapter implements AdapterParserInterface
{
    protected $parser;
    public function __construct(\WhichBrowser\Parser $parser)
    {
        $this->parser = $parser;
    }


    public function getBrowser()
    {
        return $this->parser->browser->toString();
    }

    public function getEngine()
    {
        return $this->parser->engine->toString();
    }

    public function getOs()
    {
        return $this->parser->os->toString();
    }

    public function getDevice()
    {
        return $this->parser->device->type;
    }
}
