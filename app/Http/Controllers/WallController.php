<?php


namespace App\Http\Controllers;


use App\Link;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use Ramsey\Uuid\Uuid;

class WallController
{
    public function index()
    {
        $posts = cache()->remember('get-posts', 86400, function () {
            $user = Auth::user();
            return  $user->posts()->latest()->get();;
        });

        return view('wall', ['posts' => $posts]);
    }

    public function create()
    {
        return view('create-post');
    }

    public function store(Request $request)
    {
        $request->validate([
            'text' => 'required|min:10|max:1000',
        ]);

        $text = $request->get('text');

        preg_match_all('#\bhttps?://[^,\s()<>]+(?:\([\w\d]+\)|([^,[:punct:]\s]|/))#', $text, $match);

        foreach ($match[0] as $source_link)
        {
            $link = new Link();
            $link->short_code = uniqid();
            $link->source_link = $source_link;

            Auth::user()->links()->save($link);

            $text = str_replace($source_link, sprintf('<a href="%s">%s</a>',  '/r/' . $link->short_code, '/r/' . $link->short_code), $text);
        }

        $post = new Post();
        $post->id = Uuid::uuid4()->toString();
        $post-> user_id = Auth::id();
        $post->text = $text;
        $post->save();

        return redirect()->route('wall.index');
    }

}
