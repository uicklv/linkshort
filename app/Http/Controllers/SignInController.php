<?php


namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SignInController
{
    public function index()
    {
        return view('sign-in');
    }

    public function handle(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'password' => 'required|min:8',
        ]);

        $data = $request->only(['email', 'password']);

        if (Auth::attempt($data))
        {
            return redirect()->route('home');
        }

    }

}
