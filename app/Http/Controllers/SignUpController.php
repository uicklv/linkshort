<?php


namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SignUpController
{
    public function index()
    {
        return view('sign-up');
    }

    public function handle(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:8|confirmed',
            'password_confirmation' => 'required|min:8',
        ]);

        $data = $request->only(['name', 'email', 'password']);

        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->email_verified_at = (new \DateTime())->format('Y-m-d H:i:s');
        $user->password = Hash::make($data['password']);
        $user->remember_token = Str::random(10);
        $user->save();
        if (Auth::attempt($data))
        {
            return redirect()->route('home');
        }

    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }
}
