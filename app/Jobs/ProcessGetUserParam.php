<?php

namespace App\Jobs;

use GeoIp2\Database\Reader;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mockery\Exception;


class ProcessGetUserParam implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $linkId;
    protected $ip;
    protected $browser;
    protected $engine;
    protected $os;
    protected $device;
    protected $countryCode;
    protected $cityName;
    protected $userAgent;


    /**
     * Create a new job instance.
     *

     */
    public function __construct(\App\Lib\AdapterInterface $ad, \App\AdapterParserInterface $adapt, $link)
    {
        $this->linkId = $link->id;
        $this->ip = request()->ip();
        $this->browser = $adapt->getBrowser();
        $this->engine = $adapt->getEngine();
        $this->os = $adapt->getOs();
        $this->device = $adapt->getDevice();

        $ad->parse($this->ip);
        $this->countryCode = $ad->getCountryCode();
        $this->cityName = $ad->getCityName();

        $this->userAgent = request()->userAgent();
    }

    /**
     * Execute the job.
     *
     *
     */
    public function handle()
    {
        $statistic = new \App\Statistic();
        $statistic->id = \Ramsey\Uuid\Uuid::uuid4()->toString();
        $statistic->link_id = $this->linkId;
        $statistic->ip = $this->ip;
        $statistic->browser = $this->browser;
        $statistic->engine = $this->engine;
        $statistic->os = $this->os;
        $statistic->device = $this->device;
        $statistic->country_code = $this->countryCode;
        $statistic->city_name = $this->cityName;
        $statistic->user_agent = $this->userAgent;
        $statistic->save();
    }


}


