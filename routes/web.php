<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Lib\MaxmindAdapter;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

App::singleton(\App\Lib\AdapterInterface::class, function () {
    $reader = new \GeoIp2\Database\Reader(resource_path() . '/GeoLite2/GeoLite2-City.mmdb');
    return new MaxmindAdapter($reader);

    //return new \App\Lib\IpapiAdapter();
});

App::singleton(\App\AdapterParserInterface::class, function () {
    $parser = new WhichBrowser\Parser(request()->userAgent());

    return new \App\WhichAdapter($parser);
});

Route::get('/', function () {

    $url = 'https://accounts.google.com/o/oauth2/auth';
    $params = [
        'redirect_uri' => env('OAUTH_CLIENT_REDIRECT_URI'),
        'response_type' => 'code',
        'client_id' => env('OAUTH_CLIENT_ID'),
        'scope' => 'https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile',
    ];

    $url = $url . '?' . http_build_query($params);
    return view('welcome', ['url' => $url]);
})->name('home');



Route::get('/callback', function (\Illuminate\Http\Request $request) {

    $client = new \GuzzleHttp\Client();
    $response = $client->request('POST', 'https://accounts.google.com/o/oauth2/token', [
        'form_params' =>[
            'client_id' => env('OAUTH_CLIENT_ID'),
            'client_secret' => env('OAUTH_CLIENT_SECRET'),
            'redirect_uri'  => env('OAUTH_CLIENT_REDIRECT_URI'),
	        'grant_type'    => 'authorization_code',
	        'code'          => $request->get('code'),
        ]
    ]);

    $response = $response->getBody()->getContents();
    $response_decode = json_decode($response, true);
    $params['access_token'] = $response_decode['access_token'];

    $uri = 'https://www.googleapis.com/oauth2/v1/userinfo' . '?' . http_build_query($params);

    $response = $client->request('GET', $uri);

    $user_info = json_decode($response->getBody()->getContents(), true);

    $user = \App\User::where('email', '=', $user_info['email'])->get()->first();
    if (!$user)
    {
        $user = new \App\User();
        $user->name = $user_info['name'];
        $user->email = $user_info['email'];
        $user->email_verified_at = now();
        $user->password = \Illuminate\Support\Facades\Hash::make(\Illuminate\Support\Str::random(10));
        $user->remember_token = \Illuminate\Support\Str::random(10);
        $user->save();
    }

    Auth::login($user, true);

    return redirect()->route('home');

})->name('callback');




Route::get('/r/{code}', function ($code, \App\Lib\AdapterInterface $ad, \App\AdapterParserInterface $adapt) {

    $link = \App\Link::where('short_code', $code)->get()->first();
    //$sourceLink = \App\Link::where('short_code', $code)->value('source_link');
    //$ip = request()->ip();
    //$userAgent = request()->userAgent();
    //$adapter = $ad->parse($ip);
    \App\Jobs\ProcessGetUserParam::dispatch($ad, $adapt, $link);


    //$city = $ad->getCityName();
    //$countryCode = $ad->getCountryCode();


    //$statistic = new \App\Statistic();
    //$statistic->id = \Ramsey\Uuid\Uuid::uuid4()->toString();
    //$statistic->link_id = $link->id;
    //$statistic->ip = request()->ip();
    //$statistic->browser = $adapt->getBrowser();
    //$statistic->engine = $adapt->getEngine();
    //$statistic->os = $adapt->getOs();
    //$statistic->device = $adapt->getDevice();
    //$statistic->country_code = $countryCode;
    //$statistic->city_name = $city;
    //$statistic->user_agent = $userAgent;
    //$statistic->save();
    return redirect($link->source_link);
});

Route::prefix('/admin')->middleware('auth')->group(function() {

        Route::prefix('/users')->group(function() {

            Route::get('/', '\\' . \App\Http\Controllers\UserController::class . '@index')->name('users.index');
            Route::get('/create', '\\' . \App\Http\Controllers\UserController::class . '@create')->name('users.create');
            Route::post('/', '\\' . \App\Http\Controllers\UserController::class . '@store')->name('users.store');
            Route::get('/{user}', '\\' . \App\Http\Controllers\UserController::class . '@show')->name('users.show');
            Route::get('/{user}/edit', '\\' . \App\Http\Controllers\UserController::class . '@edit')->name('users.edit');
            Route::match(['put', 'patch'] ,'/{user}/edit', '\\' . \App\Http\Controllers\UserController::class . '@update')->name('users.update');
            Route::delete('/{user}', '\\' . \App\Http\Controllers\UserController::class . '@destroy')->name('users.destroy');

        });


});



Route::get('/sign-up', '\\' . \App\Http\Controllers\SignUpController::class . '@index')->name('sign-up');
Route::post('/sign-up', '\\' . \App\Http\Controllers\SignUpController::class . '@handle')->name('handle-sign-up');

Route::get('/sign-in', '\\' . \App\Http\Controllers\SignInController::class . '@index')->name('login');
Route::post('/sign-in', '\\' . \App\Http\Controllers\SignInController::class . '@handle')->name('handle-sign-in');


Route::get('/logout', '\\' . \App\Http\Controllers\SignUpController::class . '@logout')
    ->name('logout')
    ->middleware('auth');

Route::middleware('auth')->group(function() {

    Route::get('/wall', '\\' . \App\Http\Controllers\WallController::class . '@index')->name('wall.index');
    Route::get('/create-post', '\\' . \App\Http\Controllers\WallController::class . '@create')->name('post.create');
    Route::post('/create-post', '\\' . \App\Http\Controllers\WallController::class . '@store')->name('post.store');

});
/*
Route::resources([
    'users' => '\\' . \App\Http\Controllers\UserController::class
]);
*/

