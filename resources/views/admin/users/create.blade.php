@extends('admin.layout')

@section('content')
    <form class="form-horizontal" method="POST" action="{{ route('users.store') }}">
        @csrf
    <fieldset>

    <!-- Form Name -->
    <legend>Create User</legend>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="name">Name</label>
      <div class="col-md-4">
      <input id="textinput" name="name" type="text"  class="form-control input-md" value="{{ old('name') }}">
      </div>
            @if($errors->has('name'))
                <div class="alert alert-danger" role="alert">
                    @foreach($errors->get('name') as $error)
                        <p>{{$error}}</p>
                    @endforeach
                </div>
            @endif
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="email">Email</label>
      <div class="col-md-4">
      <input id="textinput" name="email" type="text"  class="form-control input-md" value="{{old('email')}}">
      </div>
        @if($errors->has('email'))
            <div class="alert alert-danger" role="alert">
                @foreach($errors->get('email') as $error)
                    <p>{{$error}}</p>
                @endforeach
            </div>
        @endif
    </div>

    <!-- Password input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="password">Password</label>
      <div class="col-md-4">
        <input id="password" name="password" type="password" class="form-control input-md">
      </div>
        @if($errors->has('password'))
            <div class="alert alert-danger" role="alert">
                @foreach($errors->get('password') as $error)
                    <p>{{$error}}</p>
                @endforeach
            </div>
        @endif
    </div>

    <!-- Password input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="password_confirmation">Password confirmation</label>
      <div class="col-md-4">
        <input id="password_confirmation" name="password_confirmation" type="password"  class="form-control input-md">
      </div>
        @if($errors->has('password_confirmation'))
            <div class="alert alert-danger" role="alert">
                @foreach($errors->get('password_confirmation') as $error)
                    <p>{{$error}}</p>
                @endforeach
            </div>
        @endif
    </div>

    <!-- Button -->
    <div class="form-group">
      <div class="col-md-4">
        <button type="submit" id="singlebutton" name="button" class="btn btn-primary">Create</button>
      </div>
    </div>

    </fieldset>
    </form>
@endsection
