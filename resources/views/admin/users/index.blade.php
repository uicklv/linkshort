@extends('admin.layout')

@section('content')
    <a class="btn btn-success" href="{{ route('users.create') }}" role="button">Create User</a>

    @if(session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Created at</th>
            <th scope="col">Updated at</th>
            <th scope="col">Edit</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>

        @foreach($users as $user)
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->created_at}}</td>
                <td>{{$user->updated_at}}</td>
                <td><a href="{{ route('users.edit', $user->id) }}">Edit</a></td>
                <td>
                    <form action="{{route('users.destroy', $user->id)}}" method="post">
                        @csrf
                        @method('delete')
                        <input type="submit" value="X">
                    </form>
                </td>
            </tr>
        @endforeach

        </tbody>
    </table>

    {{$users->links()}}

@endsection
